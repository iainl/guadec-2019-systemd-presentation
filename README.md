# GUADEC 2019 Presentation

This repository contains our systemd presentation for GUADEC 2019.

The following commands will get you the template:

```
git clone https://gitlab.gnome.org/iainl/guadec-2019-systemd-presentation
cd guadec-2019-systemd-presentation
```

The template was provided
[here](https://gitlab.gnome.org/GNOME/presentation-templates).

## How Can I Create a Presentation?

We're using pandoc. To build:

Open your console, go to the directory where this document lies and type
`make`. This will compile the `presentation.md` into `presentation.pdf` if you
have pandoc installed.

> **Note**:
>
> You can use `make continuous` to automatically regenerate the PDF whenever you
> touch the document. For that to work you'll need to have the `inotify-tools`
> package installed.
