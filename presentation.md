% Managing GNOME sessions with `systemd`
% Iain Lane (`iain.lane@canonical.com`) and Benjamin Berg (`bberg@redhat.com`)
% August 23, 2019

# What's the problem?

---

## What's the problem?

When you log into a GNOME session from GDM, what happens to show you the shell?

> - We launch `gdm-wayland-session` (or `gdm-x-session`)
> - `gnome-session` is launched
> - `gnome-session` sets up some environment
> - `gnome-session-binary` is launched
> - `gnome-session-binary` reads `gnome.session` and begins launching XDG autostart files
> - `gnome.session` contains …

---

## What's the problem?

A definition of components required for the session to be functional:

```
RequiredComponents=org.gnome.Shell;
org.gnome.SettingsDaemon.A11ySettings;
org.gnome.SettingsDaemon.Clipboard;
org.gnome.SettingsDaemon.Color;
org.gnome.SettingsDaemon.Datetime;
org.gnome.SettingsDaemon.Housekeeping;
org.gnome.SettingsDaemon.Keyboard;
…
```

These can be satisfied from XDG autostart or from regular `.desktop` files
under `$XDG_DATA_DIRS`.

---

## What's the problem?

`gnome-session` process management is restricted:

  - User cannot control restart/launch behaviour
  - Limited process management

[//]: This makes it hard to e.g. test g-s-d services, as you cannot stop them sanely

---

## What's the problem?

Some of these things need to start before others, e.g.

  - GNOME Initial Setup initializes the home directory
  - GNOME Keyring exports environment variables for the shell

---

## What's the problem?

Some of these things need to start before others, e.g.

  - GNOME Initial Setup initializes the home directory
  - GNOME Keyring exports environment variables for the shell

`gnome-session` has the concept of 'phases' to deal with this

  - the agent starts in the `EarlyInitialization` phase
  - `gnome-shell` in the `DisplayServer` phase

This is really a crude ordering mechanism to deal with the lack of proper
dependencies.

---

## What's the problem?

Components either start or don't start. There's no way to be more specific about
this.

---

## Enter `systemd --user`

Since all of this was invented, systemd has come along.

---

## Enter `systemd --user`

We're familiar what we get from systemd managing system services:

  - Advanced process management
  - Advanced dependency management
  - Handling failure in a sensible way (e.g. by restarting the application that
    failed)
  - Possibility to sandbox services

---

## Enter `systemd --user`

There is also a *user* instance of systemd, which runs things for logged-in
users. Many of these benefits apply there. We can use it to start up GNOME.

---

## Enter `systemd --user`

Ubuntu 16.04 shipped with an (almost) entirely downstream version of this idea.
This proved that it could be done.

![](unity.png){width=70%}

---

## How to move into a systemd world

To make for a smooth transition, the current `gnome-session` startup needs to
be integrated with systemd.

 - XDG Autostart files need to be supported even with systemd
 - Non-systemd startup should remain functional
 - Allow services to launch using systemd instead

---

## How to move into a systemd world

We are still shipping XDG Autostart files for everything.

 - Still listed in `RequiredComponents`
 - `gnome-session` must not manage systemd managed components

This is solved using the `X-GNOME-HiddenUnderSystemd` flag in the desktop file.

`gnome-session` will ignore any component if it cannot launch it.

---

## How to move into a systemd world

Launch sequence on systemd:

> - `gnome-session` is started (really `gnome-session-binary`)
> - `gnome-session` uploads environment to systemd
> - `gnome-session` activates `gnome-session-{wayland,x11}@$SESSION_ID.target`
> - `gnome-session-{wayland,x11}@$SESSION_ID.target` pulls in everything else
> - `gnome-session@.target` and `gnome-session-{wayland,x11}.target` for dependencies
> - `gnome-session-manager@$SESSION_ID.service` manages the real `gnome-session` process

---

## How to move into a systemd world

Important synchronization points:

> - Startup
>   - GNOME Initial Setup runs
> - `gnome-session-pre.target` reached
>   - `gnome-session-manager.target` begins startup
>   - `gnome-keyring` launches
>   - XDG pre-display manager phases launch
> - `gnome-session-manager.target` reached
>   - `gnome-shell` launches and sets up environment
> - `gnome-session-initialized.target` reached
>   - Late phase XDG autostart launched
>   - `gnome-settings-daemon` process launch
> - `gnome-session.target` reached
> - Session is running

---

## How to move into a systemd world

Ensuring clean session shutdown with the session running outside of its scope.

 - GDM watches original `gnome-session` process
 - Original `gnome-session` opens socket
 - `gnome-session-monitor.service` opens socket
 - Either disappearing will trigger a clean session shutdown

This means that terminating the users session scope or shutting down the systemd
part will result in a clean session shutdown.

---

## How to move into a systemd world

`gnome-settings-daemon` services will only start if all dependencies are started.

 - Solved using a combination of:
   - `Requisite` -> required for launching
   - `After` -> only launched later
   - `PartOf` -> shut down at the same time
 - Allows e.g. `gsd-smartcard` to only launch in a GNOME session and when smartcard hardware was detected.

However, this causes dependency failures, and triggers the `OnFailure` target.

---

## How to move into a systemd world

`gnome-settings-daemon` has a `.target` and a `.service` file for each process.

 - `.target` has no `Onfailure`, so dependency issues don't affect the session
 - `.service` has `OnFailure` starting `gnome-session-failed.target` (will show the fail whale or shutdown)
 - `CollectMode` is set to `inactive-or-failed` so that an immediate re-login will not cause issues

---

## Session vs. User scope

The GNOME session runs **outside** any logind session. We also have one D-Bus
daemon for the user and need to upload environment variables to the systemd
user instance.

---

 - No separation of session and user scopes
   - There is just *one* D-Bus and *one* systemd environment. Without careful
     management, activated services can be told lies (e.g. if `$XDG_SESSION_ID`
     leaks).
 - We need to assume that any user can only have one graphical session

---

Fixes were needed in e.g. mutter, GDM, gnome-session to pick up the correct
session ID for logind, since they can't get it from the environment any more.

systemd has been improved to allow automatically detecting the users graphical
session.

---

This does result in some cleanup issues. We still restart the DBus daemon during
session shutdown to ensure that e.g. `evolution-data-server` will not linger.

---

## What next?

Applications should be launched outside of the service's scope:

- systemd cleans up child processes on service restart
- GNOME Shell is affected ([#1496](https://gitlab.gnome.org/GNOME/gnome-shell/issues/1496))

---

## What next?

GNOME Shell extension disabling improvements

- No fail-whale warning on wayland
- No warning for user that extensions were disabled
- Idea: Show preferences on next login
- gnome-shell issue [#1495](https://gitlab.gnome.org/GNOME/gnome-shell/issues/1495)

---

## What next?

We should think about how to deal with the 'session' vs. 'user' problems.

---

## What next?

How we can use more of the advanced features of systemd? e.g.

  - Start `gnome-settings-daemon` services in a smarter way
  - Allow Xwayland shutdown
  - Make use of sandboxing / resource usage limiting features across the
    desktop
  - …

---

## Can I try it?

Yes you can. This is in GNOME 3.33.90 and is enabled by default.

(Distributions: if you want to *disable* this, you can pass
`-Dsystemd_user=disable` when building `gnome-session`.)

---

## Thanks

Thank you to everyone who reviewed our work and provided important motivation
to get this over the line.

Special thanks to people who worked on earlier iterations of this work:
Sebastien Bacher, Ted Gould, Bastien Nocera, and Martin Pitt.

---

Any questions?

![](shell.png){width=80%}
